#!/usr/bin/env bash

( cd fileadmin ; rm -rf * )
( cd mysql ; rm -rf * )
( cd reports ; rm -rf * )
( cd typo3conf ; rm -rf * )
( cd uploads ; rm -rf * )
