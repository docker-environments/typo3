FROM php:7.2-apache-stretch

ARG typo3version

LABEL maintainer="Dr. Volker Göbbels <vmg@arachnion.de>"

RUN echo "Europe/Berlin" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        wget vim less lsof inetutils-ping \
        mailutils mailutils-common \
        libxml2-dev libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        zlib1g-dev \
        curl \
				libedit-dev \
				librecode-dev \
				libcurl4-openssl-dev \
				libc-client-dev \
				libpq-dev \
				libkrb5-dev \
				libyaml-dev \
				libcurl3 \
				libuuid1 \
				libevent-dev \
				libgearman-dev \
				libpcre3-dev \
				libxslt1-dev \
        locales \
        graphicsmagick

RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl && docker-php-ext-install imap
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ && docker-php-ext-install gd
RUN docker-php-ext-install opcache intl pdo pdo_mysql curl iconv soap curl pgsql pdo_pgsql mysqli zip fileinfo json mbstring readline recode simplexml xml xmlrpc xmlwriter xsl
RUN pecl install oauth && pecl install yaml && pecl install xdebug && docker-php-ext-enable oauth yaml xdebug
RUN a2enmod rewrite && \
    apt-get clean && apt-get -y purge \
        libxml2-dev libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        zlib1g-dev && \
    rm -rf /var/lib/apt/lists/* /usr/src/*

COPY config/php.ini /usr/local/etc/php/conf.d/
COPY config/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/
COPY config/composer /usr/local/bin
COPY config/locale.gen /etc/
RUN /usr/local/bin/composer selfupdate && /usr/sbin/locale-gen

RUN cd /var/www/html && \
    wget -O - "https://get.typo3.org/$typo3version" | tar -xzf - && \
    ln -s typo3_src-* typo3_src && \
    ln -s typo3_src/index.php && \
    ln -s typo3_src/typo3 && \
    ln -s typo3_src/_.htaccess .htaccess && \
    mkdir typo3temp && \
    mkdir typo3conf && \
    mkdir fileadmin && \
    mkdir uploads && \
    touch FIRST_INSTALL && \
    chown -R www-data. .

# Configure volumes
VOLUME /var/www/html/fileadmin
VOLUME /var/www/html/typo3conf
VOLUME /var/www/html/typo3temp
VOLUME /var/www/html/uploads
