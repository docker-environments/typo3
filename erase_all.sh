#!/usr/bin/env bash

rm -rf fileadmin/*
rm -rf mysql/*
rm -rf reports/*
rm -rf typo3conf/*
rm -rf uploads/*
